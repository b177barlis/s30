const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.byqnx.mongodb.net/b177-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Set notification for connection success or failure
// Connection to the database
let db = mongoose.connection;

// If a connection error occured, outout a message in the console

db.on("error", console.error.bind(console, "connection error"));
// If the connection is successful, output a message in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Create a Task Schema

/*const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type: String,
		// Default values are the predefined values for a field
		default: "pending"
	}
})*/

// ACTIVITY Create a user Schema

const userSchema = new mongoose.Schema({
	username : String,
	password : String
})

/*// Create Models
// Server > Schema > Database > Collection (MongoDb)
const Task = mongoose.model("Task", taskSchema);*/


// ACTIVITY Create a user model
const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found")
		}
		else{
			let newTask = new Task ({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New task created");
				}
			})
		}
	})
})


// ACTIVITY Create POST route that will access the "/signup" route that will create a user.

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate username found")
		}
		else if (req.body.username == '' || req.body.password == ''){
			return res.send("Fill in the blank field.")
		}
		else{
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New user created");
				}
			})
		}
	})
})

// Create a GET request to retrieve all the tasks

/*app.get("/tasks",(req,res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data: result
			})
		}
	})
})
*/

// Create a GET request to retrieve all the users

app.get("/signup",(req,res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				username: result

			})
		}
	})
})


app.listen(port, () => console.log(`Server running at port ${port}`));
 